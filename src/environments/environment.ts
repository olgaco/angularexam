// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase: {
        apiKey: "AIzaSyCc1lpFSM5twrZlK2qhfA8Qww16VHn9t1s",
    authDomain: "exam-e6c17.firebaseapp.com",
    databaseURL: "https://exam-e6c17.firebaseio.com",
    projectId: "exam-e6c17",
    storageBucket: "exam-e6c17.appspot.com",
    messagingSenderId: "363559486849"
  }
};
