import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRoniComponent } from './users-roni.component';

describe('UsersRoniComponent', () => {
  let component: UsersRoniComponent;
  let fixture: ComponentFixture<UsersRoniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRoniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRoniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
