
import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../users.service';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';


@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

@Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
@Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();

username;
email;

service:UsersService;

 user;

updateform = new FormGroup({
username:new FormControl(),
email:new FormControl()
});


  constructor(private route: ActivatedRoute ,service: UsersService, private router:Router, private formBuilder: FormBuilder) { 
    this.service = service;
  }

          sendData() {
          this.updateUser.emit(this.updateform.value.name);
          console.log(this.updateform.value);

          this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
            this.service.putUser(this.updateform.value, id).subscribe(
              response => {
                console.log(response.json());
                this.updateUserPs.emit();
                this.router.navigate(['/']);
              }
            );
          })
        }


          ngOnInit() {
            this.route.paramMap.subscribe(params=>{
              let id = params.get('id');
              console.log(id);
              this.service.getUser(id).subscribe(response=>{
                this.user = response.json();
                console.log(this.user);
                this.username = this.user.username
                this.email = this.user.email  
              })
            })

                      //Login without JWT
        var value = localStorage.getItem('auth');
        
        if(value == 'true'){   
        // this.router.navigate(['/']);
        }else{
          this.router.navigate(['/login']);
        }  
          }
}

