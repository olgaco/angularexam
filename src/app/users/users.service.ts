import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http'; 
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import {environment} from './../../environments/environment';
import 'rxjs/Rx';


@Injectable()
export class UsersService {

  http:Http;

  constructor(http:Http,private db:AngularFireDatabase ) {
    this.http = http;
    this.db=db;

  }

  getUsers(){
  //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get(environment.url +'users');

  }
  getDataFire(){
  return this.db.list('/products').valueChanges();
  }


  postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
   return this.http.post(environment.url+'users', params.toString(), options);
 }


  postUser(user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('username',user.username).append('email',user.email);
    return this.http.post(environment.url + 'users',params.toString(), options);     
  }
  updateUser(id,user){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('username',user.username).append('email',user.email);
    return this.http.put(environment.url + 'users/'+id, params.toString(), options);      
  }


 
  deleteUser(key){
     return this.http.delete(environment.url+ 'users/'+key);
   }
  putUser(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.put(environment.url+'users/'+ key,params.toString(), options);
  }

  getMessagesFire(){
    return this.db.list('/users').valueChanges();
  }
  
  getUser(id){
     return this.http.get(environment.url+'users/'+ id);
  }


    //---------------------------PRODUCTS---------------------------------------------------------

    getProducts(){
    //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get(environment.url+'products');

  }
    getProduct(id){
     return this.http.get(environment.url+'products/'+ id);
  }


    putProduct(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url+'products/'+ key,params.toString(), options);
  }

    updateProduct(id,product){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',product.name).append('price',product.price);
    return this.http.put(environment.url + 'products/'+id, params.toString(), options);      
  }

  searchProducts(name){
    let params = new HttpParams().append('name', name);
    let options =  {
       headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'     
        })
    } 
    return this.http.post(environment.url + '/products/search', params.toString(), options); 
  }



    login(credentials){
     let options = {
       headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('username', credentials.username).append('password',credentials.password);
    return this.http.post(environment.url+'login', params.toString(),options).map(response=>{ 
      let success = response.json().success;
      if (success == true){
        localStorage.setItem('auth','true');
      }else{
        localStorage.setItem('auth','false');        
      }
   });
  }

}
