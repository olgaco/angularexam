import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import {FormGroup,FormControl,Validators, FormBuilder  } from '@angular/forms'; //הוספה
import { UsersService } from "../users.service";
import { Router } from "@angular/router";


@Component({
  selector: 'user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); // הגדרת משתנה חדש מסוג איוונט אמיטר - סוג המידע שיועבר לא מוגדר- התשתית שתעביר לנו את המידע מאלמנט הבן לאלמנט האב
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;
    addform = new FormGroup({ // בניית מבנה נתונים בקוד המתאים אחד לאחד לטופס ההטמל. הנתונים ישמרו כמשתנים בקוד -> זהו אובייקט שדרכו תתבצע ה"תפירה" בטופס
    username:new FormControl("",Validators.required),
    email:new FormControl("",Validators.required),
    //password: new FormControl("",Validators.required),
    
  });
  sendData() {
    this.addUser.emit(this.addform.value.name);
    
    console.log(this.addform.value);
    this.service.postUsers(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }

  constructor(service:UsersService,private formBuilder:FormBuilder, private router:Router) {
    this.service = service;
   }

   ngOnInit() {
    this.addform = this.formBuilder.group({
      username:  [null, [Validators.required]],
      email: [null, [Validators.required]],
    });

              //Login without JWT
        var value = localStorage.getItem('auth');
        
        if(value == 'true'){   
        // this.router.navigate(['/']);
        }else{
          this.router.navigate(['/login']);
        } 
        
        
   }
 
 }
