import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users/users.service";
import { Router } from "@angular/router";

@Component({
  selector: 'firebase',
  templateUrl: './firebase.component.html',
  styleUrls: ['./firebase.component.css']
})
export class FirebaseComponent implements OnInit {
dataFire;
  constructor(private service:UsersService, private router:Router) { 

  }

    ngOnInit() { // פונקציה שמתעוררת ברגע שהקומפוננט נוצר, לאחר הקונסטרקטור
    this.service. getDataFire().subscribe(response=>{
    console.log(response);
    this.dataFire=response; // השמה של כל הנתונים שמגיעים מהשיטה לאובייקט בשם מסג'ס
  })


      }
}
