import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";
import { UsersService } from "./users/users.service";
import { LoginComponent } from './login/login.component';
import { FirebaseComponent } from './firebase/firebase.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from './../environments/environment';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserUpdateComponent } from './users/user-update/user-update.component';
import { UserComponent } from './users/user/user.component';
import { HomePageComponent } from './home-page/home-page.component';
import { UsersRoniComponent } from './users/users-roni/users-roni.component';
import { ProductsComponent } from './products/products.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { SearchResultsComponent } from './products/search-results/search-results.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    UsersComponent,
    NotfoundComponent,
    LoginComponent,
    FirebaseComponent,
    UserCreateComponent,
    UserUpdateComponent,
    UserComponent,
    HomePageComponent,
    UsersRoniComponent,
    ProductsComponent,
    EditProductComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
      RouterModule.forRoot([ // בניה של הראוטס
      {path:'',component:ProductsComponent}, // הראוט הראשון זה דף הבית לוקלהוסט:4200
      {path:'users', component:UsersComponent},
      {path:'user/:id', component:UserComponent},
      {path:'products', component:ProductsComponent},
      {path:'edit-product/:id', component:EditProductComponent},
      {path: 'search-results/:searchValue',component: SearchResultsComponent  },
      {path:'login', component:LoginComponent},
      {path:'firebase',component:FirebaseComponent},
      {path:'users-roni',component:UsersRoniComponent},
      {path: 'user-update/:id', component:UserUpdateComponent},
      {path: 'home-page', component:HomePageComponent},
      {path:'**',component:NotfoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])

  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
